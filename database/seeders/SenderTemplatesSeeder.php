<?php

namespace Database\Seeders;

use App\Models\SenderTemplate;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class SenderTemplatesSeeder extends Seeder
{
    private $faker;
    private $phone;

    public function __construct()
    {
        $this->faker = Factory::create();
        $this->phone = '+77019997660';
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($counter = 0; $counter <= 50; $counter++) {
            SenderTemplate::create([
                'user_id' => 1,
                'name' => $this->faker->name,
                'city_id' => $counter,
                'address' => $this->faker->address,
                'phone_number' => $this->phone,
            ]);
            $this->phone -= 1;
        }
    }
}
