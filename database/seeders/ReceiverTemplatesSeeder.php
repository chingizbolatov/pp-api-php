<?php

namespace Database\Seeders;

use App\Models\ReceiverTemplate;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class ReceiverTemplatesSeeder extends Seeder
{
    private $faker;
    private $phone;

    public function __construct()
    {
        $this->faker = Factory::create();
        $this->phone = '+77019997660';
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($counter = 0; $counter <= 50; $counter++) {
            ReceiverTemplate::create([
                'user_id' => 1,
                'name' => $this->faker->name,
                'city_id' => $counter,
                'address' => $this->faker->address,
                'phone_number' => $this->phone
            ]);
            $this->phone -= 1;
        }
    }
}
