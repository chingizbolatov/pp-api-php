<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('invoice_number')->nullable();
            $table->tinyInteger('delivery_type');
            $table->string('places');
            $table->string('weight');
            $table->integer('sender_id');
            $table->integer('receiver_id');
            $table->integer('from_city_id');
            $table->string('from_address');
            $table->integer('to_city_id');
            $table->string('to_address');
            $table->json('statuses')->nullable();
            $table->tinyInteger('is_delivered')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
