<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [\App\Http\Controllers\Api\AuthController::class, 'login']);
    Route::post('registration', [\App\Http\Controllers\Api\AuthController::class, 'registration']);
    Route::post('logout', [\App\Http\Controllers\Api\AuthController::class, 'logout']);
});

Route::group(['middleware' => 'is_auth'], function() {
    Route::resource('sender', \App\Http\Controllers\Api\SenderTemplatesController::class)
        ->only(['index', 'store', 'delete']);
    Route::resource('receiver', \App\Http\Controllers\Api\ReceiverTemplatesController::class)
        ->only(['index', 'store', 'delete']);
    Route::resource('city', \App\Http\Controllers\Api\CitiesController::class)
        ->only(['index']);
    Route::resource('order', \App\Http\Controllers\Api\OrdersController::class)
        ->only(['index', 'store']);
});
