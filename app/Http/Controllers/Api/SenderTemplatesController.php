<?php

namespace App\Http\Controllers\Api;

use App\Classes\Validations\SenderTemplatesValidationClass;
use App\Http\Controllers\Controller;
use App\Libraries\HttpCodes;
use App\Models\City;
use App\Models\SenderTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SenderTemplatesController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;
        $senderTemplates = SenderTemplate::where('user_id', $user_id)
            ->where('is_active', 1)
            ->orderBy('created_at', 'DESC')
            ->get();

        foreach ($senderTemplates as $senderTemplate) {
            $senderTemplate->city;
        }

        return $this->returnResponse(true, 'Success', $senderTemplates, HttpCodes::SUCCESS);
    }

    public function create()
    {
        $cities = City::all();
    }

    public function store(Request $request)
    {
        $validator = SenderTemplatesValidationClass::validateStoreForm($request->all());

        if (!$validator['success']) {
            return $this->returnResponse(false, 'Validation errors', $validator['errors'], HttpCodes::VALIDATION_ERROR);
        }

        $params = [
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'city_id' => $request->city_id,
            'address' => $request->address,
            'phone_number' => $request->phone_number
        ];

        try {
            $senderTemplateRecord = SenderTemplate::create($params);
        } catch (\Exception $e) {
            return $this->returnResponse(false, 'Failed', $e->getMessage(), HttpCodes::BAD_REQUEST);
        }

        return $this->returnResponse(true, 'Success', $senderTemplateRecord, HttpCodes::SUCCESS);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
