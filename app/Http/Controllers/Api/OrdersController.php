<?php

namespace App\Http\Controllers\Api;

use App\Classes\OrdersClass;
use App\Classes\StatusClass;
use App\Classes\Validations\OrderValidationClass;
use App\Http\Controllers\Controller;
use App\Libraries\HttpCodes;
use App\Models\Order;
use App\Models\ReceiverTemplate;
use App\Models\SenderTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;
        $orders = DB::select('SELECT invoice_number, 
                                            delivery_type, 
                                            places, 
                                            weight, 
                                            from_address, 
                                            to_address 
                                     FROM orders WHERE user_id = ? ORDER BY created_at DESC', [$user_id]);

        return $this->returnResponse(true, 'Success', $orders, HttpCodes::SUCCESS);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $validator = OrderValidationClass::validateStoreForm($request->all());

        if (!$validator['success']) {
            return $this->returnResponse(false, 'Validation errors', $validator['errors'], HttpCodes::VALIDATION_ERROR);
        }

        $receiverTemplate = ReceiverTemplate::findOrFail($request->receiver_id);
        $senderTemplate = SenderTemplate::findOrFail($request->sender_id);
        $ordersClass = new OrdersClass();
        $invoice_number = $ordersClass->generateInvoiceNumber();
        $statusClass = new StatusClass();
        $status = [
            'code' => 1,
            'status' => $statusClass->returnStatus(1)
        ];
        $params = [
            'invoice_number' => $invoice_number,
            'user_id' => Auth::user()->id,
            'delivery_type' => $request->delivery_type,
            'places' => $request->places,
            'weight' => $request->weight,
            'sender_id' => $request->sender_id,
            'receiver_id' => $request->receiver_id,
            'from_city_id' => $senderTemplate->city_id,
            'from_address' => $senderTemplate->address,
            'to_city_id' => $receiverTemplate->city_id,
            'to_address' => $receiverTemplate->address,
            'statuses' => json_encode($status)
        ];

        try {
            $orderRecord = Order::create($params);
        } catch (\Exception $e) {
            return $this->returnResponse(false, 'Failed', $e->getMessage(), HttpCodes::BAD_REQUEST);
        }

        return $this->returnResponse(true, 'Success', $orderRecord, HttpCodes::SUCCESS);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
