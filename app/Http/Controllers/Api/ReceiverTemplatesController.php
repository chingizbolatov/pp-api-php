<?php

namespace App\Http\Controllers\Api;

use App\Classes\Validations\ReceiverTemplatesValidationClass;
use App\Http\Controllers\Controller;
use App\Libraries\HttpCodes;
use App\Models\ReceiverTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReceiverTemplatesController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;
        $receiverTemplates = ReceiverTemplate::where('user_id', $user_id)
            ->where('is_active', 1)
            ->orderBy('created_at', 'DESC')
            ->get();

        foreach ($receiverTemplates as $receiverTemplate) {
            $receiverTemplate->city;
        }

        return $this->returnResponse(true, 'Success', $receiverTemplates, HttpCodes::SUCCESS);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $validator = ReceiverTemplatesValidationClass::validateStoreForm($request->all());

        if (!$validator['success']) {
            return $this->returnResponse(false, 'Validation errors', $validator['errors'], HttpCodes::VALIDATION_ERROR);
        }

        $params = [
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'city_id' => $request->city_id,
            'address' => $request->address,
            'phone_number' => $request->phone_number
        ];

        try {
            $receiverTemplateRecord = ReceiverTemplate::create($params);
        } catch (\Exception $e) {
            return $this->returnResponse(false, 'Failed', $e->getMessage(), HttpCodes::BAD_REQUEST);
        }

        return $this->returnResponse(true, 'Success', $receiverTemplateRecord, HttpCodes::SUCCESS);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
