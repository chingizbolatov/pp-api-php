<?php

namespace App\Http\Controllers\Api;

use App\Classes\Validations\AuthValidationClass;
use App\Http\Controllers\Controller;
use App\Libraries\HttpCodes;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'registration']]);
    }

    public function login(Request $request)
    {
        $validator = AuthValidationClass::validateLoginForm($request->all());

        if (!$validator['success']) {
            return $this->returnResponse(false, 'Validation errors', $validator['errors'], HttpCodes::VALIDATION_ERROR);
        }

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (! $token = auth()->attempt($credentials)) {
            return $this->returnResponse(false, 'Unauthorized', null, HttpCodes::UNAUTHORIZED);
        }

        return $this->respondWithToken($token, $request->remember_me);
    }

    public function registration(Request $request)
    {
        $validator = AuthValidationClass::validateRegisterForm($request->all());

        if (!$validator['success']) {
            return $this->returnResponse(false, 'Validation errors', $validator['errors'], HttpCodes::VALIDATION_ERROR);
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return $this->returnResponse(true, 'Success', $user, HttpCodes::SUCCESS);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    protected function respondWithToken($token, $remember_me)
    {
        $token_life = $remember_me ? 480 : 60;

        $data = [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * $token_life
        ];

        return $this->returnResponse(true, 'Success', $data, HttpCodes::SUCCESS);
    }
}
