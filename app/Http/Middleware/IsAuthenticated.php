<?php

namespace App\Http\Middleware;

use App\Libraries\HttpCodes;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class isAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!Auth::user()) {
            return response()->json([
                'errors' => [
                    'Unauthorized user'
                ]
            ], HttpCodes::FORBIDDEN);
        }

        return $next($request);
    }
}
