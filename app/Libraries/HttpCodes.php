<?php

namespace App\Libraries;


class HttpCodes
{
    const SUCCESS           = 200;
    const BAD_REQUEST       = 400;
    const UNAUTHORIZED      = 401;
    const FORBIDDEN         = 403;
    const VALIDATION_ERROR  = 418;
    const UNPROCESSABLE     = 422;
}
