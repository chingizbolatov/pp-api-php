<?php

namespace App\Classes;

class StatusClass
{
    private $statuses;

    public function __construct() {
        $this->statuses = [
            1 => 'Invoice Created',
            2 => 'Courier on the way',
            3 => 'Cargo in transit',
            4 => 'Delivered',
            5 => 'Recipient received the parcel'
        ];
    }

    public function returnStatus($id, $name = null)
    {
        if ($id = 5) {
            $status = $this->statuses[$id] . ' (' . $name . ')';
        } else {
            $status = $this->statuses[$id];
        }

        return $status;
    }
}
