<?php

namespace App\Classes;


use App\Models\Order;

class OrdersClass
{
    public function generateInvoiceNumber()
    {
        $orderModel = new Order();
        $last_invoice_number = $orderModel->getLastInvoiceNumber();
        $invoice_number = 'PP';

        if (empty($last_invoice_number)) {
            $invoice_number .= '99999999999999';
        } else {
            $invoice_number .= intval(ltrim($last_invoice_number, 'PP')) - 1;
        }

        return $invoice_number;
    }
}
