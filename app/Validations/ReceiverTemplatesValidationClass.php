<?php

namespace App\Classes\Validations;

class ReceiverTemplatesValidationClass extends ValidatorClass
{
    public static function validateStoreForm($data)
    {
        $rules = [
            'name' => 'required|string',
            'city_id' => 'required|integer',
            'address' => 'required|string',
            'phone_number' => 'required|string',
        ];

        return self::validate($data, $rules);
    }
}
