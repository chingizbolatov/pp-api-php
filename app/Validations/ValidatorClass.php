<?php
/**
 * Created by PhpStorm.
 * User: chingizbolatov
 * Date: 11/2/20
 * Time: 17:54
 */

namespace App\Classes\Validations;


use Illuminate\Support\Facades\Validator;

class ValidatorClass
{
    private static $success = true;
    private static $errors = [];

    public static function validate($data, $rules)
    {
        $validator = Validator::make($data, $rules);
        self::isValidatorFailed($validator);

        return self::prepareFinalData();
    }

    private static function isValidatorFailed($validator)
    {
        if ($validator->fails()) {
            self::$success = false;
            self::$errors = $validator->errors()->getMessages();
        }
    }

    private static function prepareFinalData()
    {
        return [
            'success' => self::$success,
            'errors' => self::$errors
        ];
    }
}
