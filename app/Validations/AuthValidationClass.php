<?php

namespace App\Classes\Validations;

class AuthValidationClass extends ValidatorClass
{
    public static function validateLoginForm($data)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
            'remember_me' => 'required|boolean'
        ];

        return self::validate($data, $rules);
    }

    public static function validateRegisterForm($data)
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6'
        ];

        return self::validate($data, $rules);
    }
}
