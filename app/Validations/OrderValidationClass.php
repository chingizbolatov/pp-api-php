<?php

namespace App\Classes\Validations;

class OrderValidationClass extends ValidatorClass
{
    public static function validateStoreForm($data)
    {
        $rules = [
            'delivery_type' => 'required|integer',
            'places' => 'required|integer',
            'weight' => 'required|integer',
            'sender_id' => 'required|integer',
            'receiver_id' => 'required|integer'
        ];

        return self::validate($data, $rules);
    }
}
