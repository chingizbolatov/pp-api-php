<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'user_id',
        'invoice_number',
        'delivery_type',
        'places',
        'weight',
        'sender_id',
        'receiver_id',
        'from_city_id',
        'to_city_id',
        'from_address',
        'to_address',
        'statuses',
        'is_delivered'
    ];

    public function sender() {
        return $this->hasOne('sender_templates');
    }

    public function receiver() {
        return $this->hasOne('receiver_templates');
    }

    public function fromCity() {
        return $this->hasOne('cities', 'from_city_id');
    }

    public function toCity() {
        return $this->hasOne('cities', 'to_city_id');
    }

    public function getLastInvoiceNumber()
    {
        $invoice_number = self::min('invoice_number');

        return $invoice_number;
    }
}
