<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SenderTemplate extends Model
{
    protected $table = 'sender_templates';

    protected $fillable = [
        'user_id',
        'name',
        'city_id',
        'address',
        'phone_number',
        'is_active'
    ];

    public function city() {
        return $this->hasOne(City::class, 'id', 'city_id');
    }
}
